# Go Websocket Client

A Golang SDK to help building websocket client applications, inspired by javax websocket library. The SDk is built upon https://pkg.go.dev/nhooyr.io/websocket, Golang standard library and uses [OpenTelemetry](https://opentelemetry.io/) API to instrument the SDK code. 

## Features

- SDK defines an interface for the user websocket client application with four synchronous callbacks: OnOpen, OnMessage, OnClose & OnError
- SDK provides a websocket container that manages the websocket connection and interacts with the user provided client callbacks
- Websocket container is already instrumented to track callback calls
- Websocket container has a auto-reconnect feature that, if enabled, continuously reopens closed websocket connection

## Design principles

- A single goroutine manages the websocket connection and synchronously calls callbacks. A callback must complete before any other callback to be called. If your use case requires a long-running task to process a message, it is better to do it asynchronously (start a goroutine that processes the message content in OnMessage, publish the message to a buffered channel, ...). If the websocket application implements a request-response pattern over websocket
- OnOpen callback is called synchronously and must complete before any other callback can be called. That way, the user can send and receive synchronously messages during OnOpen call. This is useful for websocket application that have subscription mechanisms.
- The websocket client interface 
- Explicit dependencies for websocket container: no magic!
- Tracer context & websocket connection propagation for callbacks

## Usage tips

### OnOpen callback

The logic contained in OnOpen callback is executed each time the websocket container (re)connects to the server. During the OnOpen callback, the user can manually and synchronously send/receive messages without the engine getting in the way. This is useful in the case you are required to send some specific messages after connecting to the server. Once OnOpen callback is executed, the engine will continuously read incoming messages and use appropriate callbacks (OnMessage, OnError, OnClose).

### Incoming message processing

Callbacks are called synchronously by the engine. Thus, no new message will be read while, for instance, a OnMessage callback is executed. For performance reasons and in order to prevent congestion, it is best to keep the complexity of the callbacks to a minimum and process messages asynchronously: You can publish messages to channels or process them with other goroutines.

### Request-Response over Websocket

Request-Response pattern can be tricky for a websocket application client because a response to a request can be mixed with other publications/responses. On top of that, you must deal with a callback based engine that executes your client application logic. Here are two ways to deal with request-response pattern:

    1. The websocket application supports user defined IDs for requests and responses (good practise). In that case, your websocket client can implement its own logic to:
        - Add user defined IDs to requests (messages sent to server over websocket)
        - Send the request
        - Store the request data and its ID in a map
        - In OnMessage callback, reconcialiate responses and requests by using the provided ID and execute further logic.
    
    2. The websocket application does not suppport user defined IDs for requests and responses. In that case, the websocket container provides a mutex that can be locked to 'pause' the engine by the time you issue your request and manually process incoming messages. Once your are done with your request (and response), you can unlock the mutex to resume the engine.

### Close the websocket connection

Use the Stop() method of the websocket container to gracefully close the websocket connection and call appropriate calllback (OnClose with ClientInitiated flag). It is not recommended to manually close the websocket connection: if the auto-reconnect feature is enabled, you will just get a OnClose call and websocket connection will be reopened.

## Licence

Apache License Version 2.0, January 2004